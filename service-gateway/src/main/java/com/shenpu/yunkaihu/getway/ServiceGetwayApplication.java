package com.shenpu.yunkaihu.getway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceGetwayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceGetwayApplication.class,args);
    }
}
