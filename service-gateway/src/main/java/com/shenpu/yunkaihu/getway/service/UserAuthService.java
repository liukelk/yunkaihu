package com.shenpu.yunkaihu.getway.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shenpu.yunkaihu.common.constant.RedisConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class UserAuthService {


    @Autowired
    private StringRedisTemplate redisTemplate;

    ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 验证token真实性
     * @param token
     * @return
     */
    public Long checkToken(String token){
        String id = redisTemplate.opsForValue().get(RedisConstant.USER_TOKEN_PREFIX + token);
        if (StringUtils.isEmpty(id) && id==null){
            //无此用户
            return null;
        }else {
            long userid = Long.parseLong(id);
            return userid;
        }

    }

}
