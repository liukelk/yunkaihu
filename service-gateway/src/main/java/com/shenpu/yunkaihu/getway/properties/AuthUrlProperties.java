package com.shenpu.yunkaihu.getway.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "auth")
public class AuthUrlProperties {

    private List<String> denyUrls;

    private List<String> loginUrls;

    private List<String> anyoneUrls;

    private String loginPage;
}
