package com.shenpu.yunkaihu.getway.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shenpu.yunkaihu.common.result.Result;
import com.shenpu.yunkaihu.common.result.ResultCodeEnum;
import com.shenpu.yunkaihu.getway.properties.AuthUrlProperties;
import com.shenpu.yunkaihu.getway.service.UserAuthService;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.*;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * 过滤器
 */
@Component
public class UserAuthFilter implements GlobalFilter {

    @Autowired
    UserAuthService userAuthService;

    @Autowired
    AuthUrlProperties properties;

    //@Autowired
    AntPathMatcher antPathMatcher = new AntPathMatcher();

    ObjectMapper objectMapper = new ObjectMapper();


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //先拿到请求
        ServerHttpRequest request = exchange.getRequest();
        //拿到请求路径
        String path = request.getPath().value();

        //TODO 判断路径 内部路径直接跳到首页
        List<String> denyUrls = properties.getDenyUrls();
        for (String url : denyUrls) {
            boolean match = antPathMatcher.match(url, path);
            if (match){
                //匹配成功内部路径跳到首页
                //如果匹配，说明是拒绝访问的 //
                Result<String> result = Result.build("", ResultCodeEnum.PERMISSION);
                return responseJson(exchange,result);

            }
        }
        //TODO 判断路径 必须要登录路径直接跳到登录页
        List<String> loginUrls = properties.getLoginUrls();
        for (String loginUrl : loginUrls) {
            boolean match = antPathMatcher.match(loginUrl, path);
            if (match){
                String token = getToken(request);
                if (StringUtils.isEmpty(token)){
                    return locationToPage(exchange,"www.baidu.com");
                }else {
                    Long userid = userAuthService.checkToken(token);
                    if (userid==null){
                        //假登录
                        return locationToPage(exchange,"www.baidu.com");
                    }else {
                        //真登陆
                        return chain.filter(exchange);
                    }
                }
            }
        }

        //判断token
        String token = this.getToken(request);
        if (StringUtils.isEmpty(token)){
            //没有登录想法直接放行
            return chain.filter(exchange);
        }else {
            //检验token真实性
            Long userid = userAuthService.checkToken(token);
            if (userid==null){
                //TODO 错误令牌
                return locationToPage(exchange,"www.baidu.com");
            }

        }
        return chain.filter(exchange);
    }

    private Mono<Void> locationToPage(ServerWebExchange exchange,String url) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.FOUND);
        response.getHeaders().setLocation(URI.create(url));
        //TODO domain("")作用域不确定 循环重定向问题
        ServerHttpRequest request = exchange.getRequest();
        HttpHeaders headers = request.getHeaders();
        String userAgent = headers.getFirst("User-Agent");
        ResponseCookie token = ResponseCookie.from("token", "").maxAge(0).domain(".com").build();
        response.getCookies().add(userAgent+"token",token);
        return response.setComplete();
    }


    private String getToken(ServerHttpRequest request) {
        // TODO 有待完善
        HttpHeaders headers = request.getHeaders();
        String userAgent = headers.getFirst("User-Agent");
        if (userAgent!=null){
            //判断请求cookie里面有没有token
            String cookieToken = this.getCookieValue(request, userAgent+"token");
            //判断请求体里面有没有token
            String headerToken = this.getHeaderValue(request,userAgent+"token");
            return StringUtils.isEmpty(cookieToken)?headerToken:cookieToken;
        }
        return null;
    }
    /**
     * 获取cookie的token
     * @param request
     * @param cookietoken
     * @return
     */
    private String getCookieValue(ServerHttpRequest request,String cookietoken){
        //拿到指定的cookie
        MultiValueMap<String, HttpCookie> cookies = request.getCookies();
        if (cookies!=null){
            HttpCookie first = cookies.getFirst(cookietoken);
            if (first!=null){
                String value = first.getValue();
                return value;
            }
        }
        return null;
    }

    /**
     * 获取请求体的token
     * @param request
     * @param headerName
     * @return
     */
    private String getHeaderValue(ServerHttpRequest request,String headerName){
        HttpHeaders headers = request.getHeaders();
        if (headers.containsKey(headerName)){
            String strings = headers.getFirst(headerName);
            return strings;
        }
        return null;
    }

    private Mono<Void> responseJson(ServerWebExchange exchange, Result<String> result) {
        ServerHttpResponse response = exchange.getResponse();
        String asString ="{}";
        try {
            asString = objectMapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        Publisher<? extends Publisher<? extends DataBuffer>> body =null;
        DataBuffer wrap = new DefaultDataBufferFactory().wrap(asString.getBytes(StandardCharsets.UTF_8));
        Mono<DataBuffer> just = Mono.just(wrap);
        response.getHeaders().setContentType(MediaType.parseMediaType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        return response.writeWith(just);
    }
}
