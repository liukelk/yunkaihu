package com.shenpu.yunkaihu.user.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.shenpu.yunkaihu.common.result.Result;
import com.shenpu.yunkaihu.model.user.UserInfo;

public interface UserInfoService {

    /**
     * 用户登录
     * @param userInfo
     * @param UserAgent
     * @return
     * @throws JsonProcessingException
     */
    Result login(UserInfo userInfo, String UserAgent) throws JsonProcessingException;

    /**
     * 用户注销
     * @param userid
     * @param userAgent
     */
    void logout(Long userid, String userAgent) throws JsonProcessingException;
}
