package com.shenpu.yunkaihu.user.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shenpu.yunkaihu.common.constant.RedisConstant;
import com.shenpu.yunkaihu.common.result.Result;
import com.shenpu.yunkaihu.common.result.ResultCodeEnum;
import com.shenpu.yunkaihu.common.util.MD5;
import com.shenpu.yunkaihu.model.user.UserInfo;
import com.shenpu.yunkaihu.user.mapper.UserInfoMapper;
import com.shenpu.yunkaihu.user.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    UserInfoMapper userInfoMapper;

    @Autowired
    StringRedisTemplate redisTemplate;

    ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Result login(UserInfo userInfo, String UserAgent) throws JsonProcessingException {
        //获取登录端口mobile  PAC登录 web PC端登录
        //String userKey = getUserAgent(UserAgent);


        String loginName = userInfo.getLoginName();
        String passwd = userInfo.getPasswd();
        //String encrypt = MD5.encrypt(passwd);
        UserInfo user = userInfoMapper.login(loginName,passwd);
        // TODO user不为null情况下 等待验证用户的状态

        if (!StringUtils.isEmpty(user)){
            //先根据用户id去redis查缓存判断用户有没有登录过
            String tokenEquipment = redisTemplate.opsForValue().get(RedisConstant.USERID_KEY_PREFIX+user.getId());
            //判断 tokenEquipment为空说明第一次登录 有缓存根据设备信息查询
            if (tokenEquipment==null){
                String token = UUID.randomUUID().toString().replace("-","");
                HashMap<String, Object> map = new HashMap<>();
                map.put("nickName",user.getNickName());
                map.put(UserAgent+"token",token);
                map.put("id",user.getId());
                //TODO 可以存放设备相关信息 目前只放用户id
                String userValue = objectMapper.writeValueAsString(user.getId());
                redisTemplate.opsForValue().set(RedisConstant.USER_TOKEN_PREFIX+token,userValue,RedisConstant.LOGIN_USER_TIMEOUT, TimeUnit.DAYS);
                Map<String, String> hashMap = new HashMap<>();
                hashMap.put(UserAgent,token);
                String tokenValue = objectMapper.writeValueAsString(hashMap);
                redisTemplate.opsForValue().set(RedisConstant.USERID_KEY_PREFIX+user.getId(),tokenValue,RedisConstant.LOGIN_USER_TIMEOUT, TimeUnit.DAYS);
                return Result.ok(map);
            }else {
                //tokenEquipment 不为空说明有缓存，查询缓存，分为两种情况一是token失效 二是token没失效
                Map<String, String> tokenValue = objectMapper.readValue(tokenEquipment, new TypeReference<Map<String, String>>() {
                });
                if (tokenValue.containsKey(UserAgent)){
                    if (tokenValue.get(UserAgent)!=null){
                        //token存在则删除旧token 踢人
                        String token = UUID.randomUUID().toString().replace("-","");
                        String orderToken = tokenValue.get(UserAgent);
                        redisTemplate.delete(RedisConstant.USER_TOKEN_PREFIX+orderToken);
                        tokenValue.remove(UserAgent);
                        tokenValue.put(UserAgent,token);
                        String asString = objectMapper.writeValueAsString(tokenValue);
                        redisTemplate.opsForValue().set(RedisConstant.USER_TOKEN_PREFIX+token,user.getId().toString(),RedisConstant.LOGIN_USER_TIMEOUT, TimeUnit.DAYS);
                        redisTemplate.opsForValue().set(RedisConstant.USERID_KEY_PREFIX+user.getId(),asString,RedisConstant.LOGIN_USER_TIMEOUT, TimeUnit.DAYS);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("nickName",user.getNickName());
                        map.put(UserAgent+"token",token);
                        map.put("id",user.getId());
                        //TODO 问题没有根据设备信息判断是否为同一设备 会出登录旧设备出现提示信息
                        return Result.build(map, ResultCodeEnum.PRODUCT_ORDER_PRICE_CHANGED);
                    }else {
                        //极端情况下 token不存在 token失效
                        String token = UUID.randomUUID().toString().replace("-","");
                        String orderToken = tokenValue.get(UserAgent);
                        redisTemplate.delete(RedisConstant.USER_TOKEN_PREFIX+orderToken);
                        tokenValue.remove(UserAgent);
                        tokenValue.put(UserAgent,token);
                        String asString = objectMapper.writeValueAsString(tokenValue);
                        redisTemplate.opsForValue().set(RedisConstant.USER_TOKEN_PREFIX+token,user.getId().toString(),RedisConstant.LOGIN_USER_TIMEOUT, TimeUnit.DAYS);
                        redisTemplate.opsForValue().set(RedisConstant.USERID_KEY_PREFIX+user.getId(),asString,RedisConstant.LOGIN_USER_TIMEOUT, TimeUnit.DAYS);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("nickName",user.getNickName());
                        map.put(UserAgent+"token",token);
                        map.put("id",user.getId());
                        //TODO 问题没有根据设备信息判断是否为同一设备 会出登录旧设备出现提示信息
                        return Result.build(map, ResultCodeEnum.PRODUCT_ORDER_PRICE_CHANGED);
                    }

                }else {
                    String token = UUID.randomUUID().toString().replace("-","");
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("nickName",user.getNickName());
                    map.put(UserAgent+"token",token);
                    map.put("id",user.getId());
                    //TODO 可以存放设备相关信息 目前只放用户id
                    tokenValue.put(UserAgent,token);
                    redisTemplate.opsForValue().set(RedisConstant.USER_TOKEN_PREFIX+token,user.getId().toString(),RedisConstant.LOGIN_USER_TIMEOUT, TimeUnit.DAYS);
                    String nweUserAgent = objectMapper.writeValueAsString(tokenValue);
                    redisTemplate.opsForValue().set(RedisConstant.USERID_KEY_PREFIX+user.getId(),nweUserAgent,RedisConstant.LOGIN_USER_TIMEOUT, TimeUnit.DAYS);


                    return Result.ok(map);
                }
            }
        }else {
            return Result.build("", ResultCodeEnum.FAIL);
        }


    }

    @Override
    public void logout(Long userid, String userAgent) throws JsonProcessingException {
        String userLogin = redisTemplate.opsForValue().get(RedisConstant.USERID_KEY_PREFIX + userid);
        Map<String, String> readValue = objectMapper.readValue(userLogin, new TypeReference<Map<String, String>>() {
        });
        String token = readValue.get(userAgent);
        redisTemplate.delete(RedisConstant.USER_TOKEN_PREFIX+token);
        readValue.remove(userAgent);
        if (readValue.size()>0&&readValue!=null){
            String asString = objectMapper.writeValueAsString(readValue);
            redisTemplate.opsForValue().set(RedisConstant.USERID_KEY_PREFIX+userid,asString,RedisConstant.LOGIN_USER_TIMEOUT,TimeUnit.DAYS);
        }else {
            redisTemplate.delete(RedisConstant.USERID_KEY_PREFIX+userid);
        }

    }

}
