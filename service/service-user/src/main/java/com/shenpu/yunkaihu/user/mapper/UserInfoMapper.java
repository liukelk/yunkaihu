package com.shenpu.yunkaihu.user.mapper;

import com.shenpu.yunkaihu.model.user.UserInfo;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoMapper {
    UserInfo login(@Param("loginName") String loginName, @Param("passwd") String passwd);

}
