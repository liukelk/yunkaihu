package com.shenpu.yunkaihu.user.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.shenpu.yunkaihu.common.result.Result;
import com.shenpu.yunkaihu.model.user.UserInfo;
import com.shenpu.yunkaihu.user.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.*;


import java.util.Map;

import static sun.net.www.protocol.http.HttpURLConnection.userAgent;

@RequestMapping("/api/user")
@RestController
public class PassportApiController {

    @Autowired
    UserInfoService userInfoService;


    @GetMapping("/logon")
    public Result login(@RequestBody UserInfo userInfo,@RequestHeader("User-Agent")String UserAgent) throws JsonProcessingException {
        //String UserAgent = request.getHeader("User-Agent");

//        Map data = (Map) result.getData();
        return userInfoService.login(userInfo,UserAgent);
    }

    @GetMapping("/logout/{userid}")
    public Result logout(@PathVariable("userid") Long userid,@RequestHeader("User-Agent")String uerAgent) throws JsonProcessingException {
        //String userAgent = request.getHeader("User-Agent");
        userInfoService.logout(userid,uerAgent);

        return Result.ok();
    }


    @GetMapping("/aaa")
    public String show(){

        return "我超帅";
    }





}
