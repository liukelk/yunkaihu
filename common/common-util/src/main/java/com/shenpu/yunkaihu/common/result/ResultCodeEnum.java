package com.shenpu.yunkaihu.common.result;

import lombok.Getter;

/**
 * 统一返回结果状态信息类
 *
 */
@Getter
public enum ResultCodeEnum {

    SUCCESS(200,"成功"),
    FAIL(201, "账号登录失败，请确认账号和密码"),
    SERVICE_ERROR(2012, "服务异常"),

    PAY_RUN(205, ""),

    LOGIN_AUTH(208, "未登陆"),
    PERMISSION(209, "没有权限"),
    SECKILL_NO_START(210, ""),
    SECKILL_RUN(211, ""),
    SECKILL_NO_PAY_ORDER(212, ""),
    SECKILL_FINISH(213, ""),
    SECKILL_END(214, ""),
    SECKILL_SUCCESS(215, ""),
    SECKILL_FAIL(216, ""),
    SECKILL_ILLEGAL(217, "请求不合法"),
    SECKILL_ORDER_SUCCESS(218, ""),
    COUPON_GET(220, ""),

    COUPON_LIMIT_GET(221, ""),
    CART_SIZE_OVERFLOW (301,""),
    ORDER_TRADENO_ERROR(400,""),
    PRODUCT_ORDER_PRICE_CHANGED(401,"您已登录新的设备"),
    STOCK_NO(402,"，")
    ;


    private Integer code;

    private String message;

    private ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
