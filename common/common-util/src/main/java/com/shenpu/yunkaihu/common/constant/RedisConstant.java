package com.shenpu.yunkaihu.common.constant;

public class RedisConstant {

    public static final String USER_TOKEN_PREFIX = "user:token:";
    public static final String USERID_KEY_PREFIX = "userid:info:";
    public static final Integer LOGIN_USER_TIMEOUT = 60*60*24*7;

}
